﻿# Yatzy Refactoring Kata

This Refactoring Kata was designed by Jon Jagger and is available in his Cyber-Dojo. See [his blog post](http://jonjagger.blogspot.co.uk/2012/05/yahtzee-cyber-dojo-refactoring-in-java.html)

Emily Bache has changed it a little, so that the rules more closely match the original game.


## Kata Description

The problem that this code is designed to solve is explained here: [Yatzy](https://sammancoaching.org/kata_descriptions/yatzy.html)

## More of Emily Bache

If you like this Kata, you may be interested in her [books](https://leanpub.com/u/emilybache) and website [SammanCoaching.org](https://sammancoaching.org)


